/// Stack
#[derive(Debug)]
pub struct Stack<T> {
    top: usize,   // stack top pointer
    data: Vec<T>, // stack container
}

impl<T> Stack<T> {
    pub fn new() -> Self {
        Self {
            top: 0,
            data: Vec::new(),
        }
    }

    pub fn push(&mut self, value: T) {
        self.data.push(value);
        self.top += 1;
    }

    pub fn pop(&mut self) -> Option<T> {
        if self.top == 0 {
            return None;
        }
        self.top -= 1;
        self.data.pop()
    }

    pub fn peek(&self) -> Option<&T> {
        if self.top == 0 {
            return None;
        }
        self.data.get(self.top - 1)
    }

    pub fn is_empty(&self) -> bool {
        0 == self.top
    }

    pub fn size(&self) -> usize {
        self.top
    }
}

#[test]
fn test_stack() {
    let mut s = Stack::new();
    s.push(1);
    s.push(2);
    s.push(3);
    println!("Top: {:?}, Size: {:?}", s.peek(), s.size());
    println!("Pop: {:?}, Size: {:?}", s.pop(), s.size());
    println!("Is_empty: {:?}, Stack: {:?}", s.is_empty(), s);
}
