use anyhow::anyhow;
use anyhow::Result;
use colored::*;
use reqwest::{header, Client, Response, Url};
use std::collections::HashMap;
use std::str::FromStr;
use structopt::StructOpt;

// 定义Httpie的Cli的主入口，它包含若干子命令
// 下面 /// 的注释是文档

/// feed get with an url and we will retrieve the response for you
#[derive(Debug, StructOpt)]
struct Get {
    /// Http 请求的URL
    #[structopt(parse(try_from_str = parse_url))]
    pub url: String,
}

fn parse_url(s: &str) -> Result<String> {
    // 这里我们仅仅检查下url是否合法
    let _url: Url = s.parse()?;

    Ok(s.into())
}

// Post 子命令，需要一个URL， 和若干个可选的key=value, 用于提供json body

/// feed get with an url and we will retrieve the response for you
#[derive(Debug, StructOpt)]
struct Post {
    /// Http 请求的Url
    #[structopt(parse(try_from_str = parse_url))]
    url: String,
    /// Http请求的body
    #[structopt(parse(try_from_str = parse_kv_pair))]
    body: Vec<KvPair>,
}

/// 命令行中的key=value, 可以通过parse_kv_pair解析成KvPair结构
#[derive(Debug, PartialEq)]
struct KvPair {
    k: String,
    v: String,
}

impl FromStr for KvPair {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        // 使用=进行split, 这会得到一个迭代器
        let mut split = s.split("=");
        let err = || anyhow!(format!("failed to parse {}", s));
        Ok(Self {
            // 从迭代器中取第一个结果作为key, 迭代器Some(T)/None
            // 我们将其转换成Ok(T)/Err(E), 然后用?处理错误
            k: (split.next().ok_or_else(err)?).to_string(),
            // 从迭代器中取第二个结果作为value
            v: (split.next().ok_or_else(err)?).to_string(),
        })
    }
}

/// 因为我们为KvPair 实现了FromStr，这里可以直接s.parse()得到kvPair
fn parse_kv_pair(s: &str) -> Result<KvPair> {
    Ok(s.parse()?)
}

// 子命令分别对应不同的HTTP方法，目前只支持get / post
#[derive(Debug, StructOpt)]
enum SubCommand {
    #[structopt(name = "get")]
    Get(Get),
    Post(Post),
    // 我们暂且不支持其他的HTTP方法
}

/// A naive httpie implementation with Rust, can you imagine how easy it is?
///
#[derive(Debug, StructOpt)]
#[structopt(name = "httpie")]
pub struct Opts {
    #[structopt(subcommand)]
    subcmd: SubCommand,
}

#[tokio::main]
async fn main() -> Result<()> {
    let opt = Opts::from_args();
    println!("{:?}", opt);
    let mut headers = header::HeaderMap::new();

    headers.insert("X-POWERED-BY", "Rust".parse()?);
    headers.insert(header::USER_AGENT, "Rust httpie".parse()?);

    let client = reqwest::Client::builder()
        .default_headers(headers)
        .build()?;

    // generate a http client
    let result = match opt.subcmd {
        SubCommand::Get(ref args) => get(client, args).await?,
        SubCommand::Post(ref args) => post(client, args).await?,
    };

    Ok(result)
}

async fn get(client: Client, args: &Get) -> Result<()> {
    let resp = client.get(&args.url).send().await?;
    Ok(print_resp(resp).await?)
}

async fn post(client: Client, args: &Post) -> Result<()> {
    let mut body = HashMap::new();
    for pair in args.body.iter() {
        body.insert(&pair.k, &pair.v);
    }
    let resp = client.post(&args.url).json(&body).send().await?;

    Ok(print_resp(resp).await?)
}

// 打印服务器版本号 + 状态码
fn print_status(resp: &Response) {
    let status = format!("{:?} {}", resp.version(), resp.status()).blue();
    println!("{}\n", status);
}

// 打印服务器返回的HTTP header
fn print_headers(resp: &Response) {
    for (name, value) in resp.headers() {
        println!("{}: {:?}", name.to_string().green(), value);
    }

    print!("\n");
}

/// 打印服务器返回的HTTP body
fn print_body(m: Option<mime::Mime>, body: &str) {
    match m {
        // 对于 `application/json` 我们pretty print
        Some(v) if v == mime::APPLICATION_JSON => {
            println!("{}", jsonxf::pretty_print(body).unwrap().cyan());
            // print_syntect(body, "json");
        }
        // 其他mime type, 直接输出
        _ => {
            println!("{}", body);
            // print_syntect(body, "html");
        }
    }
}

fn print_syntect(s: &str, ext: &str) {
    use syntect::easy::HighlightLines;
    use syntect::highlighting::ThemeSet;
    use syntect::parsing::SyntaxSet;
    use syntect::util::as_24_bit_terminal_escaped;
    use syntect::util::LinesWithEndings;

    // Load these once at the start of you program
    let ps = SyntaxSet::load_defaults_newlines();
    let ts = ThemeSet::load_defaults();
    let syntax = ps.find_syntax_by_extension(ext).unwrap();
    let mut h = HighlightLines::new(syntax, &ts.themes["base16-oncean.dark"]);
    for line in LinesWithEndings::from(s) {
        let ranges: Vec<(syntect::highlighting::Style, &str)> =
            h.highlight_line(line, &ps).unwrap();
        let escaped = as_24_bit_terminal_escaped(&ranges[..], true);
        println!("{}", escaped);
    }
}

/// 打印整个响应
async fn print_resp(resp: Response) -> Result<()> {
    print_status(&resp);
    print_headers(&resp);
    let mime = get_content_type(&resp);
    let body = resp.text().await?;
    print_body(mime, &body);

    Ok(())
}

/// 将服务器返回的content-type 解析成Mime 类型
fn get_content_type(resp: &Response) -> Option<mime::Mime> {
    resp.headers()
        .get(header::CONTENT_TYPE)
        .map(|v| v.to_str().unwrap().parse().unwrap())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_url_works() {
        assert!(parse_url("abc").is_err());
        assert!(parse_url("http://abc.xyz").is_ok());
        assert!(parse_url("https://httpbin.org/post").is_ok());
    }

    #[test]
    fn parse_kv_pair_works() {
        assert!(parse_kv_pair("a").is_err());
        assert_eq!(
            parse_kv_pair("a=1").unwrap(),
            KvPair {
                k: "a".into(),
                v: "1".into(),
            }
        );
        assert_eq!(
            parse_kv_pair("b=").unwrap(),
            KvPair {
                k: "b".into(),
                v: "".into(),
            }
        );
    }
}
