use std::fs;

// cargo run -- https://www.rust-lang.org rust.md
fn main() -> anyhow::Result<()> {
    let args = std::env::args().collect::<Vec<String>>();

    let url = &args[1];
    let output = &args[2];

    println!("Featching url: {}", url);
    let body = reqwest::blocking::get(url)?.text()?;

    println!("Converting html to markdown ...");

    let md = html2md::parse_html(&body);
    fs::write(output, md.as_bytes())?;

    println!("Converted markdown has been saved in {}!", output);

    Ok(())
}
