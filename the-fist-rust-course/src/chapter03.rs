// function as parameter and return

pub fn apply(value: i32, f: fn(i32) -> i32) -> i32 {
    f(value)
}

pub fn square(value: i32) -> i32 {
    value * value
}

pub fn cube(value: i32) -> i32 {
    value * value * value
}

// unit

pub fn pi() -> f64 {
    3.141592653
}

pub fn no_pi() {
    3.141592653;
}

// struct, enum

#[derive(Debug)]
pub enum Gender {
    Unspecified = 0,
    Female = 1,
    Male = 2,
}

#[derive(Debug, Copy, Clone)]
pub struct UserId(u64);

#[derive(Debug, Copy, Clone)]
pub struct TopicId(u64);

#[derive(Debug)]
pub struct User {
    id: UserId,
    name: String,
    gender: Gender,
}

#[derive(Debug)]
pub struct Topic {
    id: TopicId,
    name: String,
    owner: UserId,
}

#[derive(Debug)]
enum Event {
    Join((UserId, TopicId)),
    Leave((UserId, TopicId)),
    Message((UserId, TopicId, String)),
}

fn process_event(event: &Event) {
    match event {
        Event::Join((uid, _topic_id)) => println!("user {:?} joined", uid),
        Event::Leave((uid, _topic_id)) => println!("user {:?} left: {:?}", uid, _topic_id),
        Event::Message((_, _, msg)) => println!("broadcast: {}", msg),
    }
}

fn process_message(event: &Event) {
    if let Event::Message((_, _, msg)) = event {
        println!("broadcast: {}", msg);
    }
}

// while, for , loop

fn inner_fib(a: &mut u8, b: &mut u8) {
    let c = *a + *b;
    *a = *b;
    *b = c;
}

pub fn fib_loop(n: u8) -> u8 {
    let mut a = 1;
    let mut b = 1;
    let mut i = 2u8;

    loop {
        inner_fib(&mut a, &mut b);
        i += 1;
        if i >= n {
            break b;
        }
    }
}

pub fn fib_while(n: u8) -> u8 {
    let (mut a, mut b, mut i) = (1, 1, 2);
    while i < n {
        inner_fib(&mut a, &mut b);
        i += 1;
    }
    b
}

pub fn fib_for(n: u8) -> u8 {
    let (mut a, mut b) = (1, 1);

    for _ in 2..n {
        inner_fib(&mut a, &mut b);
    }
    b
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_apply() {
        assert_eq!(4, apply(2, square));
        assert_eq!(8, apply(2, cube));
    }

    #[test]
    fn test_pi_and_no_pi() {
        let is_pi = pi();
        assert_eq!(is_pi, 3.141592653);

        let is_unit1 = no_pi();
        assert_eq!(is_unit1, ());

        let is_unit2 = {
            pi();
        };

        assert_eq!(is_unit2, ());
    }

    #[test]
    fn test_struct_and_enum() {
        let alice = User {
            id: UserId(1),
            name: "Alice".into(),
            gender: Gender::Female,
        };
        let bob = User {
            id: UserId(2),
            name: "Bob".into(),
            gender: Gender::Male,
        };

        let topic = Topic {
            id: TopicId(1),
            name: "Rust".into(),
            owner: UserId(1),
        };

        let event1 = Event::Join((alice.id, topic.id));
        let event2 = Event::Leave((bob.id, topic.id));
        let event3 = Event::Message((alice.id, topic.id, "hello, World!".into()));

        // println!("event1: {:?}, event2: {:?}, event3: {:?}", event1, event2, event3);
        process_event(&event1);
        process_event(&event2);
        process_event(&event3);
        process_message(&event3);
    }

    #[test]
    fn test_fib() {
        let n = 10;
        assert_eq!(55, fib_for(n));
        assert_eq!(55, fib_loop(n));
        assert_eq!(55, fib_while(n));
    }

    #[test]
    fn test_rust_range() {
        let arr = [1, 2, 3, 4];
        assert_eq!(arr[..], [1, 2, 3, 4]);
        assert_eq!(arr[0..=1], [1, 2]);
    }
}
