/// #07｜所有权-值的生杀大权到底在谁手上？
///
///
/// ## 变量在函数调用时发生了什么呢？
///
/// 动态数组因为大小在编译器无法确定，所以放在堆上，并且在栈上
/// 有一个包含了长度和容量的胖指针指向堆上的内存。
///
/// ### Rust所有权的规则：
///
/// - 一个值只能被一个变量所拥有，这个变量被称为所有者。
/// - 一个值同一时刻只能有一个所有者，也就说不能有两个变量拥有相同的值。
/// 所以对于变量赋值、参数传递、函数返回等行为，旧的所有者会把值的所有权
/// 转移给新的所有者，以便保证单一所有者的约束。
/// - 当所有者离开作用域，其拥有的值被丢弃，内存得到释放。
///
/// 所有权规则，解决了谁真正拥有数据的生杀大权问题，让堆上数据的多重引用不复存在，
/// 这是它最大的优势。
///
/// 对于存储在栈上的简单数据，避免所有权转移之后不能访问的情况，手动复制麻烦的问题，
/// Rust提供了Copy Trait.
///
/// ### Copy 语义:
/// Copy 语义: 符合Copy语义的类型，在赋值或者传参的时候，值会自动按位拷贝。
/// 当移动一个值，如果值的类型实现了Copy trait，就会自动Copy语义进行拷贝，否则使用Move语义进行移动。
///
/// ### Move 语义：
/// Move 语义：赋值或者传参会导致Move，所有权被转移，一旦所有权转移，之前的变量就不能访问。
///
/// - 原生类型，包括函数，不可变引用和裸指针实现了Copy,
/// - 数组和元组，如果其内部的数据结构实现了Copy, 那么他们也实现了Copy
/// - 可变引用没有实现Copy
/// - 非固定大小的数据结构，没有实现Copy
///
/// trait是Rust用于定义数据结构行为的接口。如果一个数据结构实现了Copy trait，那么它在赋值、函数调用
/// 以及函数返回时会执行Copy语义，值会被按位拷贝一份，而非移动。
pub mod inner_chapter07 {

    #[test]
    fn test_find_pos() {
        fn find_pos(data: Vec<u32>, v: u32) -> Option<usize> {
            for (pos, item) in data.iter().enumerate() {
                if *item == v {
                    return Some(pos);
                }
            }

            None
        }

        let data = vec![10, 42, 9, 8];
        let v = 42;
        if let Some(pos) = find_pos(data, v) {
            println!("Found {} at {}", v, pos);
        }
    }

    #[test]
    fn test_sum_func() {
        fn sum(data: Vec<u32>) -> u32 {
            data.iter().fold(0, |acc, x| acc + x)
        }

        let data = vec![1, 2, 3, 4];
        let data1 = data;
        println!("sum of data1: {}", sum(data1));
        // println!("data1: {:?}", data1); // error1
        // println!("sum of data: {}", sum(data)); // error2
    }

    fn is_copy<T: Copy>() {}

    #[test]
    fn test_types_impl_copy_trait() {
        fn type_impl_copy_trait() {
            is_copy::<bool>();
            is_copy::<char>();

            // all ixx and uxx, usize/isuze, fxx implement Copy trait
            is_copy::<i8>();
            is_copy::<u8>();
            is_copy::<i32>();
            is_copy::<u32>();
            is_copy::<u64>();
            is_copy::<i64>();
            is_copy::<i128>();
            is_copy::<u128>();
            is_copy::<usize>();
            is_copy::<isize>();

            // function (actually a pointer) is Copy
            is_copy::<fn()>();

            // raw pointer is Copy
            is_copy::<*const String>();
            is_copy::<*mut String>();

            // immutable reference is Copy
            is_copy::<&[Vec<u8>]>();
            is_copy::<&String>();

            // arraty/tuple with values which is Copy is Copy
            is_copy::<[u8; 4]>();
            is_copy::<(&str, &str)>();
        }

        fn type_not_impl_copy_trait() {
            // unsized or dynamic sized type is not Copy
            // is_copy::<str>();
            // is_copy::<[u8]>();
            // is_copy::<Vec<u8>>();
            // is_copy::<String>();

            // mutable reference is not Copy
            // is_copy::<&mut String>();

            // array / tuple with values that not Copy is not Copy
            // is_copy::<[Vec<u8>; 4]>();
            // is_copy::<(String, u32)>();
        }

        type_impl_copy_trait();
        type_not_impl_copy_trait();
    }
}
