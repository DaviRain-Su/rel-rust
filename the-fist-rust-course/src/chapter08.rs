/// # 08｜所有权： 值的借用是如何工作的？
///
/// 当我们不希望值的所有权被转移，又因为没有实现Copy trait而无法使用Copy 语义，
/// 这时我们可以使用引用。来借用数据。
/// ## Borrow 语义
/// Borrow语义允许一个值的所有权，在不发生转移的情况下，被其他上下文使用。Borrow语义通过
/// 引用语法(& 或者&mut)来实现。
/// ***在Rust中,借用和引用是一个概念，***只不过在其他语言中引用的意义和Rust不同，所以Rust提供了新概念
/// “借用”。
///
/// 在其他语言中，引用是一种别名，多个引用拥有对值的无差别的访问权限，本质上是共享了所有权；而在Rust下
/// 所有的引用都只是借用了“临时使用权”，它并不破坏值的单一所有权约束。
///
/// 默认情况下，Rust的借用都是只读的。
///
/// ## 只读借用 / 引用
///
/// Rust没有传引用的概念，Rust所有的参数传递的都是值，不管是Copy还是Move，所以在Rust中，你必须显式地把
/// 某个数据的引用，传给另一个函数。
///
/// Rust的引用实现了Copy trait, 所以按照Copy语义，这个引用会被复制一份交给要调用的函数。对这个函数来说，
/// 它并不拥有数据本身，数据只是临时借给它使用，所有权还在原来的拥有者哪里。
///
/// 在Rust中，引用是一等公民，和其他数据地位相等。
/// ## 借用的生命周期及其约束
///
/// 堆变量的生命周期不具备任意长短的灵活性，因为堆上内存的生死存亡，跟栈上的所有者
/// 牢牢绑定。而栈上内存的生命周期，又跟栈的生命周期相关，所以我们核心只需关注调用栈的生命周期。
/// ## 可变借用 / 引用
///
/// 同时有一个可变引用和若干多个只读引用会有问题的。
/// ## Rust的限制
/// Rust对引用的限制
/// - 在一个作用域内，仅允许一个活跃的可变引用。所谓活跃，就是真正被用来修改数据的可变引用，如果只是定义了，却没有使用
/// 或者当作只读引用使用，不算活跃。
/// - 在一个作用域内，活跃的可变引用（写）和只读引用（读）是互斥的，不能同时存在。
/// ## 小结
/// - 一个值在同一个时刻只有一个所有者。当所有者离开作用有，其拥有的值会被丢弃。
/// 赋值或者传承会导致值move，所有权被转移，一旦所有权转移，之前的变量就不会被访问。
/// - 如果值实现了Copy trait,那么赋值或者传参会使用Copy语义，相应的值会被按位拷贝，产生新的值。
/// - 一个值可以有多个只读引用
/// - 一个值可以有唯一一个活跃的可变引用。可变引用（写）和只读引用（读）是互斥的关系。就像并发下数据的读写互斥一样。
/// - 引用的生命后期不能超出值的生命周期。
pub mod inner_chapter08 {
    /// sum of vector u32
    pub fn sum(data: &Vec<u32>) -> u32 {
        // 值的地址会改变吗？引用的地址会改变吗？
        // 只读引用实现了Copy trait，这就意味这，传参都会产生新的拷贝。
        println!("addr of value: {:p}, addr of ref: {:p}", data, &data);
        data.iter().fold(0, |acc, x| acc + x)
    }

    #[test]
    fn test_sum() {
        let data = vec![1, 2, 3, 4];
        let data1 = &data;

        //值的地址是什么？引用的地址又是什么？
        println!(
            "addr of value: {:p}({:p}), addr of data {:p}, data1: {:p}",
            &data, data1, &&data, &data1
        );
        println!("sum of data1: {}", sum(data1));

        // 堆上的数据地址是什么?
        println!(
            "addr of items: [{:p}, {:p}, {:p}, {:p}]",
            &data[0], &data[1], &data[2], &data[3]
        );
        println!("data1: {:?}", data1); // error1
        println!("sum of data: {}", sum(&data)); // error2
    }

    #[test]
    fn test_local_ref_1() {
        fn local_ref() -> i32 {
            let a = 42;
            a
        }

        let r = local_ref();
        println!("r: {:p}", &r);
    }

    #[test]
    fn test_local_ref_2() {
        let mut data: Vec<&u32> = Vec::new();
        let v = 42;
        data.push(&v);
        println!("data: {:p}", &data);
    }

    #[test]
    fn test_local_ref_3() {
        fn push_local_ref<'a>(v: u32, data: &mut Vec<&'a u32>) {
            // data.push(&v.clone());
        }

        let mut data: Vec<&u32> = Vec::new();
        let v = 42;
        push_local_ref(v, &mut data);
        println!("data: {:?}", data);
    }

    #[test]
    fn test_mut_ref_1() {
        let mut data = vec![1, 2, 3];

        // for item in data.iter_mut() {
        // data.push(*item + 1);
        //}

        let mut data = vec![1, 2, 3];
        let data1 = vec![&data[0]];
        println!("data[0]: {:p}", &data[0]);

        for i in 0..100 {
            data.push(i);
        }

        println!("data[0]: {:p}", &data[0]);
        // println!("boxed: {:p}", &data1);
    }

    #[test]
    fn test_realloc_vec() {
        fn extend_vec(v: &mut Vec<i32>) {
            // Vec<T> 堆内存里T的个数是指数增长的，我们让它恰好push33个元素，
            // capacity 会变成 64
            (2..34).into_iter().for_each(|i| v.push(i));
        }

        fn print_vec<T>(name: &str, data: Vec<T>) {
            let p: [usize; 3] = unsafe { std::mem::transmute(data) };
            // 打印Vec<T> 的堆地址，capacity, len
            println!("{}: 0x{:x}, {}, {}", name, p[0], p[1], p[2]);
        }

        let mut v = vec![1];

        let v1: Vec<i32> = Vec::with_capacity(8);

        print_vec("v1", v1);

        println!("heap start: {:p}", &[0] as *const i32);

        extend_vec(&mut v);

        println!("new heap start: {:p}", &v[0] as *const i32);

        print_vec("v", v);
    }
}
