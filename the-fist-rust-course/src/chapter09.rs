/// # 09 | 所有权： 一个值可以有多个所有者吗？
///
/// 当有些情况需要，多个操作去访问。
///
/// ## Rust提供了运行时动态检查
/// Rust在编译时，处理大部分使用场景，保证安全性和效率；运行时，处理
/// 无法在编译时处理的场景，会牺牲一部分效率，提高灵活性。
///
/// Rust有引用计数的智能指针：Rc(Reference counter)和Arc(Atomic reference counter).
/// ## Rc
/// 对一个Rc结构进行clone(), 不会将内部的数据复制，只会增加引用计数。
///
pub mod inner_chapter09 {
    #[test]
    fn test_rc_counter() {
        use std::rc::Rc;
        let a = Rc::new(1);
        println!("rc = {:?}", a);
    }
}
