#![allow(dead_code)]

pub mod chapter03;
pub mod chapter07;
pub mod chapter08;
pub mod chapter09;
pub mod chapter10;
pub mod chapter11;
pub mod chapter12;
pub mod chapter13;
pub mod chapter14;
pub mod chapter15;
pub mod chapter16;
pub mod chapter17;
pub mod chapter18;
pub mod chapter19;
pub mod chapter20;
pub mod chapter21;
pub mod chapter22;
#[cfg(test)]
mod tests {}
